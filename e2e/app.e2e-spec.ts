import { EfimerumPage } from './app.po';

describe('efimerum App', () => {
  let page: EfimerumPage;

  beforeEach(() => {
    page = new EfimerumPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
