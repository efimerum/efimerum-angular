import { Component, OnInit, Inject } from '@angular/core';
import { UsersService } from "./services/users.service";
import {FirebaseRef, AngularFire} from "angularfire2";
import {PushNotificationsService} from "angular2-notifications";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor (private _usersService: UsersService, private _angularFire: AngularFire, @Inject(FirebaseRef) private firebase,
               private _pushNotification: PushNotificationsService) {
  }

  ngOnInit () {
    this._pushNotification.requestPermission();
    this._usersService.isLogged().subscribe(user => {
      if (user !== null && user.provider !== null) {
        if (user.provider === 4) {
          this.firebase.database().ref('/photosPostedByUser/' + user.uid).on('child_changed',
            snapshot => {
              this._angularFire.database.list('/photos/' + snapshot.key).subscribe(data => {
                let thumbnailSrc;
                data.forEach(object => {
                  if (object.$key === 'thumbnailData') {
                    thumbnailSrc = object.url;
                    this._pushNotification.create('Congrats!', {body: 'Someone liked your photo!', icon: thumbnailSrc}).subscribe(
                      res => console.log(res),
                      err => console.log(err)
                    );
                  }
                });
              });
            },
            err => console.log(err));
        }
      }else {

      }
    }, err => console.log(err));
  }


}
