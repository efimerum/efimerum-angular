import { Injectable, Inject } from '@angular/core';
import { AngularFire, FirebaseRef } from 'angularfire2';

@Injectable()
export class UsersService {

  constructor(private _angularFire: AngularFire, @Inject(FirebaseRef) private firebase) { }

  loginAsAnonymous() {
    return this.firebase.auth().signInAnonymously();
  }

  login(email: string, pwd: string) {
    return this._angularFire.auth.login({ email: email, password: pwd });
  }

  logout() {
    this._angularFire.auth.logout();
    this.firebase.auth().signInAnonymously();
  }

  isLogged() {
    return this._angularFire.auth;
  }

  signUp(email: string, pwd: string) {
      return this._angularFire.auth.createUser({ email: email, password: pwd });
  }
}
