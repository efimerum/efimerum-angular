import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AngularFire } from 'angularfire2';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

@Injectable()
export class PhotosService {

  private photoIndex = new Subject<number>();
  private showViewer = new Subject<boolean>();
  private tagFilter = new Subject<string>();
  private orderFilter = new Subject<string>();

  _photoIndex: Observable<number> = this.photoIndex.asObservable();
  _showViewer: Observable<boolean> = this.showViewer.asObservable();
  _tagFilter: Observable<string> = this.tagFilter.asObservable();
  _orderFilter: Observable<string> = this.orderFilter.asObservable();

  constructor(private _http: Http, private _angularFire: AngularFire) {
  }

  /**
   * HTTP and Firebase functions
   */

  // Get all photos and order by
  public getPhotos(order?: string) {

    if (typeof order === 'undefined') {
      return this._angularFire.database.list('/photos', {query: {orderByChild: 'sha1'}});
    }
    return this._angularFire.database.list('/photos', {query: {orderByChild: order}});
  }

  // Get all labels
  public getLabels() {
    return this._angularFire.database.list('/labels/EN/');
  }

  // Get photos by tag
  public getPhotosByTag(tag: string) {
    return this._angularFire.database.list('/photosByLabel/EN/' + tag, {query: {orderByChild: 'sha1'}});
  }

  // Get photos by user
  public getPhotosByUser(userId: string) {
    return this._angularFire.database.list('/photosPostedByUser/' + userId, {query: {orderByChild: 'sha1'}});
  }

  public getPhotosLikedByUser(userId: string) {
    return this._angularFire.database.list('/photosLikedByUser/' + userId, {query: {orderByChild: 'sha1'}});
  }

  // Post for like
  public setLike(userId: string, imageKey: string, latitude: number, longitude: number) {

    const body = {
      idToken: userId,
      photoKey: imageKey,
      latitude: latitude,
      longitude: longitude,
    };

    return this._http.post('https://efimerum-48618.appspot.com/api/v1/likes', body).map((res: Response) => { res.json(); } );
  }

  // Report content of photo
  public reportPhoto(userId: string, photoKey: string, reportCode: string) {
    const body = {
      idToken: userId,
      photoKey: photoKey,
      reportCode: reportCode
    };

    return this._http.post('https://efimerum-48618.appspot.com/api/v1/reportPhoto', body).map((res: Response) => { res.json(); } );
  }


  /**
   * Sending data as Observable
   */
  public getPhotoIndex(index: number) {
    this.photoIndex.next(index);
  }

  public showImageViewer(show: boolean) {
    this.showViewer.next(show);
  }

  public setTagFilter(tag: string) {
    this.tagFilter.next(tag);
  }

  public setOrder(order: string) {
    this.orderFilter.next(order);
  }
}
