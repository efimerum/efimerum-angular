/**
 * Created by skurt on 24/2/17.
 */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchByTag'
})

export class SearchByTagPipe implements PipeTransform {
  transform(items: any[], value: string) {

    return items.filter(function (image: any) {
      if (typeof value === 'undefined') {
        return image;
      }
      if (value === '') {
        return image;
      }
      if (typeof image.labels === 'undefined') {
        image.labels = {EN: {}};
      }

      for (const key in image.labels.EN) {
        if (key.toLowerCase().startsWith(value.toLowerCase())) {

          return image;
        }
      }

    } );
  }
}
