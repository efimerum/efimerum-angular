/**
 * Created by skurt on 5/4/17.
 */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeleft'
})

export class TimeleftPipe implements PipeTransform {
  transform(items: any[]) {

    return items.filter(function (image: any) {
      const date = image['expirationDate'] * 1000;
      const future: Date = new Date(date);
      const diff = Math.floor((future.getTime() - new Date().getTime()) / 1000);

      if (diff > 0) {
        return image;
      }
    });
  }
}
