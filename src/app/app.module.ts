/**
 * Angular modules
 */
import { BrowserModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

/**
 * 3º Party Libraries
 */
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';
import { ResponsiveModule } from 'ng2-responsive';
import { MaterializeModule } from 'angular2-materialize';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { ShareButtonsModule } from 'ng2-sharebuttons';

/**
 * Components
 */
import { AppComponent } from './app.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { ViewerComponent } from './components/viewer/viewer.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { LogoutComponent } from './components/logout/logout.component';
import { LoginSignupComponent } from './components/login-signup/login-signup.component';
import { MapLikesComponent } from './components/map-likes/map-likes.component';
import { PhotosComponent } from './components/photos/photos.component';
import { SearchBoxComponent } from './components/search-box/search-box.component';
import { HeaderBarComponent } from './components/header-bar/header-bar.component';
import { FooterBarComponent } from './components/footer-bar/footer-bar.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';

/**
 * Services
 */
import { PhotosService} from './services/photos.service';
import { UsersService} from './services/users.service';

/**
 * Filters
 */
import { SearchByTagPipe } from './pipes/search-by-tag.pipe';
import {TimeleftPipe} from './pipes/timeleft.pipe';

/**
 * Configs
 */

import { AppRoutingModule } from './app.routing';
import { UserDropdownComponent } from './components/user-dropdown/user-dropdown.component';
import { OrderDropdownComponent } from './components/order-dropdown/order-dropdown.component';
import {PushNotificationsModule} from 'angular2-notifications';


export class MyHammerConfig extends HammerGestureConfig  {
  overrides = <any> {
    'swipe': { velocity: 0.4, threshold: 20 },
    'pinch': { enable: true }
  };
  events: string[] = ['pinch'];
}

const myFirebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
};


/**
 * Module
 */
@NgModule({
  declarations: [
    AppComponent,
    GalleryComponent,
    ViewerComponent,
    SearchBoxComponent,
    SearchByTagPipe,
    TimeleftPipe,
    UserProfileComponent,
    LogoutComponent,
    LoginSignupComponent,
    MapLikesComponent,
    PhotosComponent,
    HeaderBarComponent,
    FooterBarComponent,
    LoginComponent,
    SignupComponent,
    UserDropdownComponent,
    OrderDropdownComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterializeModule,
    FormsModule,
    Ng2AutoCompleteModule,
    HttpModule,
    PushNotificationsModule,
    ResponsiveModule,
    ShareButtonsModule.forRoot(),
    AngularFireModule.initializeApp ({
      apiKey: 'AIzaSyDZcz1tbmPrkSYDOoTOxq4_LIVCjzytSKs',
      authDomain: 'efimerum-48618.firebaseapp.com',
      databaseURL: 'https://efimerum-48618.firebaseio.com',
      storageBucket: 'efimerum-48618.appspot.com'
    }, myFirebaseAuthConfig)
  ],
  providers: [
    PhotosService,
    UsersService,
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
