import { Component, OnInit, EventEmitter } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { MaterializeAction } from 'angular2-materialize';

@Component({
  selector: 'header-bar',
  templateUrl: './header-bar.component.html',
  styleUrls: ['./header-bar.component.css']
})
export class HeaderBarComponent implements OnInit {

  private token: any;
  modalActions = new EventEmitter<string|MaterializeAction>();

  constructor(private _usersService: UsersService) {
  }

  ngOnInit() {
    this._usersService.isLogged().subscribe((user) => {
      if (user !== null && user.provider !== null) {
        if (user.provider === 4) {
          this.token = user.auth.refreshToken;
        }
      }else {
        this.token = null;
      }
    }, err => console.log(err));
  }

  openModal() {
    this.modalActions.emit({action: 'modal', params: ['open']});
  }
  closeModal() {
    this.modalActions.emit({action: 'modal', params: ['close']});
  }
}
