import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'login-signup',
  templateUrl: './login-signup.component.html',
  styleUrls: ['./login-signup.component.css']
})
export class LoginSignupComponent {

  @Output() close = new EventEmitter<any>();

  constructor() {
  }

  public closeModal() {
    this.close.emit(false);
  }

}
