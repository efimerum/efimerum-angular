import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { AngularFire } from 'angularfire2';

@Component({
  selector: 'user-dropdown',
  templateUrl: './user-dropdown.component.html',
  styleUrls: ['./user-dropdown.component.css']
})
export class UserDropdownComponent implements OnInit {

  @Output() logoutClicked = new EventEmitter<boolean>();
  public avatar = '../../../assets/team-placeholder.jpg';
  private user: any;


  constructor(private _usersService: UsersService, private _angularFire: AngularFire) {
  }
  ngOnInit() {
    this._usersService.isLogged().subscribe((user) => {
      if (user !== null && user.provider !== null) {
        if (user.provider === 4) {
          if (user.auth.uid !== null) {
            this._angularFire.database.list('/users/' + user.auth.uid).subscribe(data => {
              data.forEach(object => {
                if (object.$key === 'profileImageURL') {
                  this.avatar = object.$value;
                }
              });
            });
          }else {
            this.avatar = '../../../assets/team-placeholder.jpg';
          }
        }
      }
    }, err => console.log(err));
  }

  public logout() {
    this.logoutClicked.emit(true);
  }

}
