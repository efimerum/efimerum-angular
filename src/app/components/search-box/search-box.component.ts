import {  Component, ChangeDetectorRef, OnChanges, SimpleChanges, EventEmitter, Output, Input } from '@angular/core';
import { animate, trigger, transition, state, style } from '@angular/animations';
import { PhotosService } from '../../services/photos.service';

@Component({
    selector: 'search-box',
    templateUrl: './search-box.component.html',
    styleUrls: ['./search-box.component.css'],
    animations: [
        trigger('searchinput', [
            state('void', style({
                'transform': 'translateX(100%)',
                'opacity': 0
            })),
            state('hidden', style({
                'transform': 'translateX(100%)',
                'opacity': 0
            })),
            state('shown', style({
                'transform': 'translateX(0%)',
                'background-color': '#999999',
                'opacity': 1
            })),
            transition('hidden <=> shown', animate('600ms cubic-bezier(0.77, 0, 0.175, 1)'))
        ])
    ]
})
export class SearchBoxComponent implements OnChanges {

    @Output() onSearch: EventEmitter<any> = new EventEmitter();

    @Input() labels: any;

    searchInputAnimationState = 'hidden';

    constructor(private _changeDetectorRef: ChangeDetectorRef, private _photosService: PhotosService) {

    }

    ngOnChanges(changes: SimpleChanges) {
    }

    showSearchInput(): void {
      if (this.searchInputAnimationState === 'shown') {
        this.searchInputAnimationState = 'hidden';
      }else if (this.searchInputAnimationState === 'hidden') {
        this._changeDetectorRef.detectChanges();
        this.searchInputAnimationState = 'shown';
      }


    }

    hideSearchInput(e: FocusEvent): void {
        this.searchInputAnimationState = 'hidden';
        setTimeout(() => ((e.target || e.srcElement) as HTMLInputElement).value = '', 650);
    }

    notifySearch(e: KeyboardEvent) {
        if ((e.keyCode || e.which) === 13) {
            this.onSearch.emit(((e.target || e.srcElement) as HTMLInputElement).value);
            this._photosService.setTagFilter(((e.target || e.srcElement) as HTMLInputElement).value);
        }
    }

    filterGallery(value: any) {
      this._photosService.setTagFilter(value);
    }
}
