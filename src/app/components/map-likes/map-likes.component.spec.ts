import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapLikesComponent } from './map-likes.component';

describe('MapLikesComponent', () => {
  let component: MapLikesComponent;
  let fixture: ComponentFixture<MapLikesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapLikesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapLikesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
