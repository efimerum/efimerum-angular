import { Component, Input, OnChanges } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'map-likes',
  templateUrl: './map-likes.component.html',
  styleUrls: ['./map-likes.component.css']
})
export class MapLikesComponent implements OnChanges {

  @Input() uid: string;
  private url;

  constructor(private sanitizer: DomSanitizer) {
  }

  ngOnChanges() {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl('https://efimerum-48618.appspot.com/api/v1/likesMap?photoUUID=' + this.uid);
  }
}
