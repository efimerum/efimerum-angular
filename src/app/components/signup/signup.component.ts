import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UsersService } from '../../services/users.service';
import { AngularFire } from 'angularfire2';

@Component({
  selector: 'sign-up',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {

  public failedSign = false;
  public err: Error;

  @Output() closeClicked: EventEmitter<any> = new EventEmitter();

  constructor(private _usersService: UsersService, private _angularFire: AngularFire) { }

  public signUp(form: FormGroup) {
    this.failedSign = false;
    this._usersService.signUp(form.value.mail, form.value.pass).then(user => {
      this._angularFire.database.object('/users/' + user.uid).set({name: form.value.name, email: form.value.mail});
      this.closeClicked.emit(true);
    }, err => {
      this.failedSign = true;
      this.err = err;
    });
  }

  public closeModal() {
    this.closeClicked.emit(true);
  }
}
