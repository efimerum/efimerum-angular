import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import User = firebase.User;
import { UsersService } from '../../services/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'log-out',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  @Output() close = new EventEmitter<any>();

  constructor(private _usersService: UsersService) { }

  ngOnInit() {
  }

  public logout() {
    this._usersService.logout();
    this.close.emit(true);
  }

  public closeModal() {
    this.close.emit(true);
  }

}
