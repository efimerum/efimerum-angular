import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'log-in',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  public failed = false;
  public err: Error;

  @Output() closeClicked: EventEmitter<any> = new EventEmitter();

  constructor(private _usersService: UsersService) { }

  public login(form: FormGroup) {
    this.failed = false;
    this._usersService.login(form.value.email, form.value.pwd).then(() => {
      this.closeClicked.emit(true);
    }, err => {
      console.log(err);
      this.failed = true;
      this.err = err;
    });

  }

  public closeModal() {
    this.closeClicked.emit(true);
  }
}
