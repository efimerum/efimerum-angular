import {Component, Input, EventEmitter, Output, Inject, ViewChild, ElementRef, OnInit} from '@angular/core';
import { animate, trigger, transition, state, style } from '@angular/animations';
import { PhotosService } from '../../services/photos.service';
import { UsersService } from '../../services/users.service';
import { FirebaseRef, AngularFire } from 'angularfire2';
import { MaterializeAction } from 'angular2-materialize';

@Component({
    selector: 'photo-viewer',
    templateUrl: './viewer.component.html',
    styleUrls: ['./viewer.component.css'],
    host: {
        '(document:keydown)': 'onKeydown($event)',
    },
    animations: [
        trigger('imageTransition', [
            state('enterFromRight', style({
                opacity: 1,
                transform: 'translate(0px, 0px)'
            })),
            state('enterFromLeft', style({
                opacity: 1,
                transform: 'translate(0px, 0px)'
            })),
            state('leaveToLeft', style({
                opacity: 0,
                transform: 'translate(-100px, 0px)'
            })),
            state('leaveToRight', style({
                opacity: 0,
                transform: 'translate(100px, 0px)'
            })),
            transition('* => enterFromRight', [
                style({
                    opacity: 0,
                    transform: 'translate(30px, 0px)'
                }),
                animate('250ms 500ms ease-in')
            ]),
            transition('* => enterFromLeft', [
                style({
                    opacity: 0,
                    transform: 'translate(-30px, 0px)'
                }),
                animate('250ms 500ms ease-in')
            ]),
            transition('* => leaveToLeft', [
                style({
                    opacity: 1
                }),
                animate('250ms ease-out')]
            ),
            transition('* => leaveToRight', [
                style({
                    opacity: 1
                }),
                animate('250ms ease-out')]
            )
        ]),
        trigger('showViewerTransition', [
            state('true', style({
                opacity: 1
            })),
            state('void', style({
                opacity: 0
            })),
            transition('void => *', [
                style({
                    opacity: 0
                }),
                animate('1000ms ease-in')]
            ),
            transition('* => void', [
                style({
                    opacity: 1
                }),
                animate('500ms ease-out')]
            )
        ])
    ]
})

export class ViewerComponent implements OnInit {
    @Input() images: any[] = [{}];
    @Output() removed = new EventEmitter<any[]>();
    @ViewChild('likeButton') likeButton: ElementRef;
    public image: any;
    private likedImages: any[] = [];
    private userImages: any[] = [];
    private currentIdx = 0;
    public showViewer: boolean;
    public leftArrowVisible = true;
    public rightArrowVisible = true;
    public transform: number;
    public Math: Math;
    public showMap = false;
    private userId: string;
    private latitude: number;
    private longitude: number;
    public logged: boolean;
    public facebookImg = '../../../assets/facebook.png';
    public twitterImg = '../../../assets/twitter.png';
    public favoriteIcon = '<i class="material-icons">favorite_border</i>';
    public modalActions = new EventEmitter<string|MaterializeAction>();
    public liked = false;
    public mapIcon = '<i class="material-icons red">favorite</i>';

    constructor(private _photosService: PhotosService, private _usersService: UsersService,
                @Inject(FirebaseRef) private firebase, private _angularFire: AngularFire) {
    }
    ngOnInit() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
          this.latitude = position.coords.latitude;
          this.longitude = position.coords.longitude;
        }, err => console.log(err));
      }

      this._usersService.isLogged().subscribe((user) => {
        if (user !== null && user.provider != null) {
          if (user.provider === 4 && this.firebase.auth().currentUser != null) {
            this.firebase.auth().currentUser.getToken().then(idToken => {
              this.userId = idToken;
              this.logged = true;
              this._angularFire.database.list('/photosLikedByUser/' + user.uid).subscribe(data => {
                  this.likedImages = data;
                },
                err => console.log(err)
              );
              this._angularFire.database.list('/photosPostedByUser/' + user.uid).subscribe(data => {
                  this.userImages = data;
                },
                err => console.log(err)
              );
            }).catch(error => console.log(error));
          }else {
            this.logged = false;
          }
        }
      }, err => console.log(err));
      this._photosService._photoIndex.subscribe(newIndex => {
        this.currentIdx = newIndex;
        this.images.forEach((image) => image['active'] = false);
        this.images[this.currentIdx]['active'] = true;
        this.transform = 0;
      }, err => console.log(err));
      this._photosService._showViewer.subscribe(show => {
        this.showViewer = show;
        this.favoriteIcon = '<i class="material-icons">favorite_border</i>';
        this.liked = false;
        if (this.logged === true) {
          this.userImages.forEach(image => {
            if (typeof image.$key !== 'undefined' && image.$key === this.images[this.currentIdx].key) {
              this.liked = true;
              this.favoriteIcon = '<i class="material-icons red">favorite</i>';
            }
          });
          this.likedImages.forEach(image => {
            if (typeof image.$key !== 'undefined' && image.$key === this.images[this.currentIdx].key) {
              this.liked = true;
              this.favoriteIcon = '<i class="material-icons red">favorite</i>';
            }
          });
        }
      });
      this.Math = Math;
    }

    public get leftArrowActive(): boolean {
        return this.currentIdx > 0;
    }

    public get rightArrowActive(): boolean {
        return this.currentIdx < this.images.length - 1;
    }

    public pan(swipe: any) {
        this.transform = swipe.deltaX;
    }

    public onResize() {
        this.images.forEach((image) => {
            image['viewerImageLoaded'] = false;
            image['active'] = false;
        });
        this.updateImage();
    }

    public imageLoaded(image) {
        image['viewerImageLoaded'] = true;
    }

    /**
     * direction (-1: left, 1: right)
     */
    public navigate(direction: number, swipe?: any) {
        this.favoriteIcon = '<i class="material-icons">favorite_border</i>';
        this.liked = false;
        this.showMap = false;
        if ((direction === 1 && this.currentIdx < this.images.length - 1) ||
            (direction === -1 && this.currentIdx > 0)) {

            if (direction === -1) {
                this.images[this.currentIdx]['transition'] = 'leaveToRight';
                this.images[this.currentIdx - 1]['transition'] = 'enterFromLeft';
            }else {
                this.images[this.currentIdx]['transition'] = 'leaveToLeft';
                this.images[this.currentIdx + 1]['transition'] = 'enterFromRight';
                this.image = this.images[this.currentIdx];
                this.doLike();
            }
            this.images.splice(this.currentIdx, 1);
            this.removed.emit(this.images);
            this.currentIdx += direction;

            if (this.logged === true) {
              this.userImages.forEach(image => {
                if (image.$key === this.images[this.currentIdx].key) {
                  this.liked = true;
                  this.favoriteIcon = '<i class="material-icons red">favorite</i>';
                }
              });
              this.likedImages.forEach(image => {
                if (image.$key === this.images[this.currentIdx].key) {
                  this.liked = true;
                  this.favoriteIcon = '<i class="material-icons red">favorite</i>';
                }
              });
            }
            if (swipe) {
                this.hideNavigationArrows();
            } else {
                this.showNavigationArrows();
            }
            this.updateImage();
        }
    }

    private hideNavigationArrows() {
        this.leftArrowVisible = false;
        this.rightArrowVisible = false;
    }

    public showNavigationArrows() {
        this.leftArrowVisible = true;
        this.rightArrowVisible = true;
    }

    public closeViewer() {
        this.images.forEach((image) => image['transition'] = undefined);
        this.images.forEach((image) => image['active'] = false);
        this.showMap = false;
        this._photosService.showImageViewer(false);
    }

    private updateImage() {
        setTimeout(() => {
            this.images[this.currentIdx]['active'] = true;
            this.images.forEach((image) => {
                if (image !== this.images[this.currentIdx]) {
                    image['active'] = false;
                    this.transform = 0;
                }
            });
        }, 500);
    }

    private onKeydown(event: KeyboardEvent) {
        const prevent = [37, 39, 27, 36, 35]
            .find(no => no === event.keyCode);
        if (prevent) {
            event.preventDefault();
        }

        switch (prevent) {
            case 37:
                // navigate left
                this.navigate(-1, false);
                this.showMap = false;
                break;
            case 39:
                // navigate right
                this.navigate(1, false);
                this.showMap = false;
                break;
            case 27:
                // esc
                this.closeViewer();
                break;
          case 36:
                this.images[this.currentIdx]['transition'] = 'leaveToRight';
                this.currentIdx = 0;
                this.images[this.currentIdx]['transition'] = 'enterFromLeft';
                this.updateImage();
                break;
            case 35:
                this.images[this.currentIdx]['transition'] = 'leaveToLeft';
                this.currentIdx = this.images.length - 1;
                this.images[this.currentIdx]['transition'] = 'enterFromRight';
                this.updateImage();
                break;
        }
    }

  public showMapLike() {
      if (this.showMap === false) {
        this.mapIcon = '<i class="material-icons">place</i>';
      }else {
        this.mapIcon = '<i class="material-icons">close</i>';
      }
     this.image = this.images[this.currentIdx];
     this.showMap = !this.showMap;
  }

  public doLike() {
      if (this.logged === true && this.liked === false) {
        this.image = this.images[this.currentIdx];

        this._photosService.setLike(this.userId, this.image.key, this.latitude, this.longitude)
          .subscribe(res => {
            this.favoriteIcon = '<i class="material-icons red">favorite</i>';
            this.navigate(1);
          }, err => console.log(err));
      }else {
        // this.modalActions.emit({action: 'modal', params: ['open']});
      }
  }

  public closeModal(value: boolean) {
    if (value === true) {
      this.modalActions.emit({action: 'modal', params: ['close']});
    }else {
      this.modalActions.emit({action: 'modal', params: ['close']});
    }
  }
}
