import { Component, OnInit, Inject } from '@angular/core';
import { PhotosService } from '../../services/photos.service';
import { FirebaseRef } from 'angularfire2';
import { Subscription } from 'rxjs/Subscription';
import * as GeoFire from 'geofire';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.css']
})
export class PhotosComponent implements OnInit {

  images: any[] = [];
  fullImages: any[] = [];
  labels: any[] = [];
  positionFilter: any[] = [];
  keys: any[] = [];
  filter = '';
  loading = true;
  imagesSubscription: Subscription;
  inicialOrder: string;
  actualLabel = 'All';
  firebaseDatabase: any;
  geofire: any;
  userCoords: any[] = [];

  constructor(private _photosService: PhotosService, @Inject(FirebaseRef) private firebase) {}

  ngOnInit() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.userCoords = [position.coords.latitude, position.coords.longitude];
        this.firebaseDatabase = this.firebase.database().ref('/geofirePhotos');
        this.geofire = new GeoFire(this.firebaseDatabase);
        const geoQuery = this.geofire.query({
          center: this.userCoords,
          radius: 30
        });

        geoQuery.on('key_entered', (key) => {
          this.positionFilter.push(key);
        });
      }, err => console.log(err));
    }

    this._photosService.getLabels().subscribe((data) => {
      this.labels.push('');
      this.labels.push('Todas las fotos');

      data.forEach((label) => {
        const name = label.$key.toString();
        this.labels.push(name);
      });
    });

    this._photosService._tagFilter.subscribe((tag) => {

      this.filter = tag;
      this.actualLabel = tag;
      if (tag === '' || tag === 'Todas las fotos') {
        this.filter = '';
        this.actualLabel = 'All';
      }
    }, err => console.log(err));

    this.imagesSubscription = this._photosService.getPhotos().subscribe(
      data => {

        this.fullImages = data;
        this.fullImages.forEach((image) => {
          image['key'] = image.$key;
          image['galleryImageLoaded'] = false;
          image['active'] = false;
          image['viewerImageLoaded'] = false;
          image['srcAfterFocus'] = '';
          image['blurFilter'] = false;
        });
        this.images = this.fullImages;

        this.loading = false;
        this.imagesSubscription.unsubscribe();

      },
      err => console.log(err),
      () => undefined);

    this._photosService._orderFilter.subscribe(order => {

      this.filter = '';
      this.inicialOrder = order;

      if (order === 'expirationDate1') {
        order = 'expirationDate';
      }else if (order === 'geofire') {
        order = 'sha1';
      }
      this.loading = true;
      this.imagesSubscription.unsubscribe();
      this.imagesSubscription = this._photosService.getPhotos(order).subscribe(data => {
        this.fullImages = data;
        this.images = [];

        this.fullImages.forEach((image) => {
          image['key'] = image.$key;
          image['galleryImageLoaded'] = false;
          image['active'] = false;
          image['viewerImageLoaded'] = false;
          image['srcAfterFocus'] = '';
          image['blurFilter'] = false;
        });

        if (this.inicialOrder === 'geofire') {
          const filter = [];
          this.fullImages.forEach((image) => {
            this.positionFilter.forEach((key) => {
              if (image.key === key) {
                filter.push(image);
              }
            });
          });

          this.images = filter;
          this.loading = false;

        }else {
          if (this.inicialOrder !== 'expirationDate1') {
            this.fullImages.reverse();
          }
          this.images = this.fullImages;

          this.loading = false;
        }
        this.images.slice();
        this.imagesSubscription.unsubscribe();
      }, err => console.log(err));
    });
  }

  public removedItem(items: any[]) {
    this.images = items;
  }
  public reloadData() {
    this.loading = true;
    this.images = [];
    this.imagesSubscription = this._photosService.getPhotos().subscribe(
      data => {

        this.fullImages = data;
        this.fullImages.forEach((image) => {
          image['key'] = image.$key;
          image['galleryImageLoaded'] = false;
          image['active'] = false;
          image['viewerImageLoaded'] = false;
          image['srcAfterFocus'] = '';
          image['blurFilter'] = false;
        });
        this.images = this.fullImages;

        this.loading = false;
        this.imagesSubscription.unsubscribe();

      },
      err => console.log(err),
      () => undefined);
  }

}
