import { Component, OnInit } from '@angular/core';
import { PhotosService } from '../../services/photos.service';

@Component({
  selector: 'order-dropdown',
  templateUrl: './order-dropdown.component.html',
  styleUrls: ['./order-dropdown.component.css']
})
export class OrderDropdownComponent {

  orderFilter = 'Random';

  constructor(private _photosService: PhotosService) { }

  changeDropdown (value: string) {
    this.orderFilter = value;
    if (value === 'Random') {
      const random = Math.floor(Math.random() * 3) + 1;
      switch (random) {
        case 1:
          value = 'sha1';
          break;
        case 2:
          value = 'sha256';
          break;
        case 3:
          value = 'randomString';
          break;
        default:
          value = 'sha1';
          break;
      }
      value = 'sha2';
    }else if (value === 'Popularity') {
      value = 'numOfLikes';
    }else if (value === 'More time') {
      value = 'expirationDate';
    }else if (value === 'Less time') {
      value = 'expirationDate1';
    }else if (value === 'Near you') {
      value = 'geofire';
    }
    this._photosService.setOrder(value);
  }

}
