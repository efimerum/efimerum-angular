import { Component, ViewChild, ElementRef, ChangeDetectorRef, OnInit, Input, SimpleChanges,
  OnChanges, Output, EventEmitter, Inject } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { PhotosService } from '../../services/photos.service';
import { Observable } from 'rxjs/Observable';
import { FirebaseRef, AngularFire } from 'angularfire2';


@Component({
    selector: 'photo-gallery',
    templateUrl: './gallery.component.html',
    styleUrls: ['./gallery.component.css'],
})
export class GalleryComponent implements OnInit, OnChanges {

    // Variables
    private gallery: any[] = [];
    private actualGallery: any[] = [];
    private countdown = '../../../assets/firelow.png';
    private observableSubs: Subscription;
    public blurFilter = false;

    // Inputs
    @Input() providedImageMargin = 0;
    @Input() providedImageSize = 10;
    @Input()images: any[];

    // Output events
    @Output() viewerChange = new EventEmitter<boolean>();
    @Output() viewerRemovedImage = new EventEmitter<any[]>();

    // Native elements
    @ViewChild('galleryContainer') galleryContainer: ElementRef;
    @ViewChild('countdownContainer') countdownContainer: ElementRef;

    constructor(private ChangeDetectorRef: ChangeDetectorRef, private _angularFire: AngularFire,
                private _photosService: PhotosService, @Inject(FirebaseRef) private firebase) {
    }

    public ngOnInit() {
      // Detect likes
      this.firebase.database().ref('/photos').on('child_changed', snapshot => {
        this.images.forEach(image => {
          if (snapshot.key === image.key) {
            this._angularFire.database.list('/photos/' + image.key).subscribe(data => {
              data.forEach(object => {
                if (object.$key === 'numOfLikes') {
                  image['numOfLikes'] += 1;
                }else if (object.$key === 'expirationDate') {
                  image['expirationDate'] = object.$value;
                }
              });
              this.render(this.images);
              this.render(this.images);
              this.ChangeDetectorRef.detectChanges();
            });
          }
        });
      }, err => console.log(err));
    }

    public ngOnChanges(changes: SimpleChanges) {
      this.render(this.images);
    }

    public openImageViewer(img, render) {
      if (img['blurFilter'] === false) {
        this._photosService.getPhotoIndex(this.images.indexOf(img));
        this._photosService.showImageViewer(true);
      }
    }

    private render(images: any[]) {

      // Add Countdown Observables to images
        this.images.forEach((image) => {
          const date = image['expirationDate'] * 1000;
          const future: Date = new Date(date);
          let diff: any;

          this.observableSubs = Observable.interval(1000).map((x) => {
            diff = Math.floor((future.getTime() - new Date().getTime()) / 1000);
          }).subscribe((x) => {
            this.countdown = this.dhms(diff);
            // Remove images
            if (diff <= 0) {
              // Blur effect on image when diff < 0
               image['blurFilter'] = true;
            }
          });
        });

        // Set images for each row
        this.gallery = [];
        let tempRow = [images[0]];
        let rowIndex = 0;
        let i = 0;
        for (i; i < images.length; i++) {
            while (images[i + 1] && this.shouldAddCandidate(tempRow, images[i + 1])) {
                i++;
            }
            if (images[i + 1]) {
                tempRow.pop();
            }
            this.gallery[rowIndex++] = tempRow;
            tempRow = [images[i + 1]];
        }

        // Scale gallery
        this.scaleGallery();
    }

    private shouldAddCandidate(imgRow: any[], candidate: any): boolean {
        const oldDifference = this.calcIdealHeight() - this.calcRowHeight(imgRow);
        imgRow.push(candidate);
        const newDifference = this.calcIdealHeight() - this.calcRowHeight(imgRow);

        return Math.abs(oldDifference) > Math.abs(newDifference);
    }

    private calcRowHeight(imgRow: any[]) {
        const originalRowWidth = this.calcOriginalRowWidth(imgRow);

        const ratio = (this.getGalleryWidth() - (imgRow.length - 1) * this.calcImageMargin()) / originalRowWidth;
        const rowHeight = imgRow[0].thumbnailData.height * ratio;

        return rowHeight;
    }

    private calcImageMargin() {
        const galleryWidth = this.getGalleryWidth();
        const ratio = galleryWidth / 1920;
        return Math.round(Math.max(1, this.providedImageMargin * ratio));
    }

    private calcOriginalRowWidth(imgRow: any[]) {
        let originalRowWidth = 0;
        imgRow.forEach((img) => {
            const individualRatio = this.calcIdealHeight() / img.thumbnailData.height;
            img.thumbnailData.width = img.thumbnailData.width * individualRatio;
            img.thumbnailData.height = this.calcIdealHeight();
            originalRowWidth += img.thumbnailData.width;
        });

        return originalRowWidth;
    }

    private calcIdealHeight() {
        return this.getGalleryWidth() / (80 / this.providedImageSize) + 100;
    }

    private getGalleryWidth() {
        if (this.galleryContainer.nativeElement.clientWidth === 0) {
            return this.galleryContainer.nativeElement.scrollWidth;
        }
        return this.galleryContainer.nativeElement.clientWidth;
    }

    private scaleGallery() {
        let maximumGalleryImageHeight = 0;

        this.gallery.forEach((imgRow, index) => {
            const originalRowWidth = this.calcOriginalRowWidth(imgRow);

            if (imgRow !== this.gallery[this.gallery.length - 1]) {
                const ratio = (this.getGalleryWidth() - (imgRow.length - 1) * this.calcImageMargin()) / originalRowWidth;

                imgRow.forEach((img) => {
                    img['width'] = img.thumbnailData.width * ratio;
                    img['height'] = img.thumbnailData.height * ratio;
                    maximumGalleryImageHeight = Math.max(maximumGalleryImageHeight, img['height']);
                  img['srcAfterFocus'] = img.thumbnailData.url;
                });
            } else {
                imgRow.forEach((img) => {
                    img.width = img.thumbnailData.width;
                    img.height = img.thumbnailData.height;
                    maximumGalleryImageHeight = Math.max(maximumGalleryImageHeight, img['height']);
                  img['srcAfterFocus'] = img.thumbnailData.url;
                });
            }
        });
        this.ChangeDetectorRef.detectChanges();
    }

    removedImage(items: any[]) {
      this.images = items;
      this.viewerRemovedImage.emit(this.images);
      this.render(this.images);
    }

    private dhms(t) {
      let days, hours, minutes, seconds;
      days = Math.floor(t / 86400);
      t -= days * 86400;
      hours = Math.floor(t / 3600) % 24;
      t -= hours * 3600;
      minutes = Math.floor(t / 60) % 60;
      t -= minutes * 60;
      seconds = t % 60;

      // Set flame for countdown
      if (typeof this.countdownContainer !== 'undefined') {
        if (days < 1 && hours < 1 && minutes < 5) {
          this.countdownContainer.nativeElement.style.width = 20;
          this.countdownContainer.nativeElement.style.height = 20;
          return '../../../assets/firemedium.png';
        }else if (days < 1 && hours < 1 && minutes < 1) {
          this.countdownContainer.nativeElement.style.width = 25;
          this.countdownContainer.nativeElement.style.height = 25;
          return '../../../assets/firehigh.png';
        }else {
          this.countdownContainer.nativeElement.style.width = 15;
          this.countdownContainer.nativeElement.style.height = 15;
          return '../../../assets/firelow.png';
        }
      }
  }
}
