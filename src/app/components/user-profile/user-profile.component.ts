import { Component, OnInit, EventEmitter } from '@angular/core';
import { PhotosService } from '../../services/photos.service';
import { AngularFire } from 'angularfire2';
import { UsersService } from '../../services/users.service';
import { MaterializeAction } from 'angular2-materialize';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  public likes: any[] = [];
  public photos: any[] = [];
  public avatarImage = '../../../assets/team-placeholder.jpg';
  public name: string;
  public log = false;
  public modalActions = new EventEmitter<string|MaterializeAction>();
  private likesSubscription: Subscription;
  private photosSubscription: Subscription;
  private avatarSubscription: Subscription;

  constructor(private _photosService: PhotosService, private _angularFire: AngularFire, private _usersService: UsersService,
              private _router: Router) {
  }

  ngOnInit() {
    this._usersService.isLogged().subscribe((user) => {
      if (user !== null) {
        if (user.provider === 4) {
          this.log = true;
          this.avatarSubscription = this._angularFire.database.list('/users/' + user.uid).subscribe(data => {
            data.forEach(object => {
              if (object.$key === 'name') {
                this.name = object.$value;
              }else if (object.$key === 'profileImageURL') {
                this.avatarImage = object.$value;
              }
            });
          });
          this.photosSubscription = this._photosService.getPhotosByUser(user.uid).subscribe((data) => {
            this.photos = data;
            this.photos.forEach((image) => {
              image['key'] = image.$key;
              image['galleryImageLoaded'] = false;
              image['active'] = false;
              image['viewerImageLoaded'] = false;
              image['srcAfterFocus'] = '';
              image['blurFilter'] = false;
            });
            this.photos.slice();
            this.photosSubscription.unsubscribe();
          });
          this.likesSubscription = this._photosService.getPhotosLikedByUser(user.uid).subscribe((data) => {
            this.likes = data;
            this.likes.forEach((image) => {
              image['key'] = image.$key;
              image['galleryImageLoaded'] = false;
              image['active'] = false;
              image['viewerImageLoaded'] = false;
              image['srcAfterFocus'] = '';
              image['blurFilter'] = false;
            });
            this.likes.slice();
            this.likesSubscription.unsubscribe();
          });
        }else {
          this.log = false;
          this.modalActions.emit({action: 'modal', params: ['open']});
        }
      }else {
        this.log = false;
        this.modalActions.emit({action: 'modal', params: ['open']});
      }
    });
  }

  public closeModal(value: boolean) {
    if (value === true) {
      this.modalActions.emit({action: 'modal', params: ['close']});
    }else {
      this.modalActions.emit({action: 'modal', params: ['close']});
      this._router.navigate(['/photos']);
    }
  }

}
