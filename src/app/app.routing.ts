/**
 * Created by skurt on 24/2/17.
 */
/**
 * Angular modules
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/**
 * Components
 */
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { PhotosComponent } from './components/photos/photos.component';

const routes: Routes = [
  {
    path: 'photos',
    component: PhotosComponent,
  },
  {
    path: 'profile',
    component: UserProfileComponent,
  },
  {
    path: '**',
    redirectTo: '/photos'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
