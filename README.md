# Efimerum

Aplicación de Efimerum en Angular con Angular-CLI.

## Uso

```
git clone https://skurt23@bitbucket.org/efimerum/efimerum-web.git
cd Efimerum
npm install
ng serve
```

La aplicación se servirá en **localhost:4200**.

## Bugs

* El dropdown y los tabs del modal dejan, de funcionar después de entrar en el perfil de un usuario pero no da error y no se le ha encontrado 
solución. Parece un bug de la librería angular2-materialize o del framework CSS de materialize usándolo con Angular.
